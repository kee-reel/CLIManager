#include "plugin.h"

#include <QSocketNotifier>
#include <iostream>
#include "linenoise/linenoise.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this),
	m_notifier(new QSocketNotifier(fileno(stdin), QSocketNotifier::Read, this))
{
	m_elements.reset(new ReferenceInstancesList<ICLIElement>());
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
	},
	{
		{INTERFACE(IApplication), m_application}
	},
	{
		{INTERFACE(ICLIElement), m_elements}
	}
	);

	m_parser.setApplicationDescription("Test helper");
	m_parser.addHelpOption();
	m_parser.addVersionOption();
	m_parser.addPositionalArgument("source", QCoreApplication::translate("main", "Source file to copy."));
	m_parser.addPositionalArgument("destination", QCoreApplication::translate("main", "Destination directory."));

	connect(&*m_notifier, SIGNAL(activated(int)), this, SLOT(readCommand()));
}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	processInitialArgs(m_application->getCommandLineArguments());
}

void Plugin::onReferencesListUpdated(Interface interface)
{
	m_commandToElement.clear();
	for(auto& e : *m_elements)
	{
		m_commandToElement.insert(e->getComandName(), e);
	}
}

void Plugin::processInitialArgs(const QStringList& args)
{
	if(args.length() > 1)
	{
		processCommand(args.sliced(1));
		//		QCoreApplication::quit();
	}
}

void Plugin::readCommand()
{
	std::string stdStr;
	std::getline(std::cin, stdStr);
	QString str(stdStr.data());
	processCommand(str.split(' '));
}

void Plugin::processCommand(const QStringList &args)
{
	if(args[0] == "help")
	{
		for(auto iter = m_commandToElement.begin(); iter != m_commandToElement.end(); ++iter)
		{
			qDebug() << "Plugin::processCommand" << QString("%1 %2").arg(iter.key(), iter.value()->help());
		}
		return;
	}
	auto iter = m_commandToElement.find(args[0]);
	if(iter == m_commandToElement.end())
	{
		qDebug() << "Unknown command";
		return;
	}
	iter.value()->handle(args.sliced(1));
}
